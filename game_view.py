import pygame, sys, time
import game_model as gm
from pygame.locals import *

FLOOD_CADENCE = 5
TILE_SIZE = 50

S_MAP = [[0,1,2,3],
         [0,1,2,2],
         [0,1,1,1],
         [0,0,0,0]]

B_MAP = [[0,0,1,1,2,3,3,3,4,5],
         [0,0,1,1,2,3,3,3,4,5],
         [0,0,1,1,2,3,3,3,4,5],
         [0,0,1,1,2,3,2,2,4,5],
         [0,0,1,1,2,3,1,3,4,5],
         [0,1,1,1,1,1,3,3,4,5],
         [0,2,1,1,2,3,3,3,4,5],
         [0,3,1,1,2,3,3,1,4,5],
         [0,4,1,0,2,3,3,1,4,5],
         [0,0,1,0,2,3,3,1,1,0]]

MAP = B_MAP

def tile_color(flooded, map, row, col):
    clr_r = 32 * map[row][col]
    clr_g = 32 * map[row][col]
    clr_b = 32 * map[row][col]

    if (row, col) in flooded:
        clr_b = 255
        
    return (clr_r, clr_g, clr_b)

mh = len(MAP[0])
mw = len(MAP)

flooded = [(row,col) for row in range(mh) for col in range(mw) if MAP[row][col] == 0]
shore = list(flooded)

turn_count = 0
flood_depth = 0

vh = mh * TILE_SIZE
vw = mw * TILE_SIZE

pygame.init()
view = pygame.display.set_mode((vh,vw))
pygame.display.set_caption('My First Game')

while True:
    if turn_count % FLOOD_CADENCE == 0:
        flood_depth += 1
    
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    shore, flooded = gm.flood(MAP, flooded, shore, flood_depth)
       
    for row in range(mh):
        for col in range(mw):
            clr = tile_color(flooded, MAP, row, col)
            pygame.draw.rect(view, clr, (col*TILE_SIZE, row*TILE_SIZE, TILE_SIZE,TILE_SIZE))

    pygame.display.update()
    turn_count += 1
    time.sleep(1)
