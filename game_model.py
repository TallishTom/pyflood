############################################################
# Rules for flooding
############################################################

def neibours(tile, map):
    ns = []
    if tile[0] > 0:
        ns.append((tile[0] - 1, tile[1]))
    if tile[0] < len(map) - 1:
        ns.append((tile[0] + 1, tile[1]))
    if tile[1] > 0:
        ns.append((tile[0], tile[1] - 1))
    if tile[1] < len(map[0]) - 1:
        ns.append((tile[0], tile[1] + 1))
    return ns

def flood_tile(map, tile, depth):
    # Already know it is next to flooded area
    # my be higher
    # will flood instantly to full depth
    if map[tile[0]][tile[1]] < depth:     # newly flooded
        return [tile]
    else:                     # still above water
        return []
    
def unflooded_neibours(neibours, flooded):
    return [tile for tile in neibours if tile not in flooded]

def tile_on_shore(tile, map, flooded):
    ns = neibours(tile, map)
    return unflooded_neibours(ns, flooded) > 0

def flood_neibours(map, shore, flooded, depth, tile):
    ns = neibours(tile, map)
    ufns = unflooded_neibours(ns, flooded)
    newly_flooded = []
    for neibour in ufns:
        nts = flood_tile(map, neibour, depth)
        newly_flooded.extend(nts)

    return newly_flooded

def flood(map, flooded, shore, flood_depth):
    """
    Takes the existing flooding state and generates the new shoreline by adding newly flooded tiles to
    and, taking newly "offshored" tiles from, the shore
    """
    new_shore = []
    for tile in shore:
        if tile_on_shore(tile, map, flooded):
            new_shore.extend(flood_neibours(map, shore, flooded, flood_depth, tile))
        else:
            shore.remove(tile)

    
    return (shore + new_shore, flooded + new_shore)


