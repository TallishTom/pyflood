import pytest
import game_model as g

class TestNeibours:
    def test_single_cell_neibours(self):
        assert len(g.neibours((0,0), [[]])) == 0

    def test_two_horizontal_cells_neibours(self):
        assert len(g.neibours((0,0), [[0,0]])) == 1

    def test_two_vertical_cells_neibours(self):
        assert len(g.neibours((1,0), [[0],[0]])) == 1

    def test_four_square_corner_cell_neibours(self):
        assert len(g.neibours((0,0), [[0,0],[0,0]])) == 2

    def test_nine_square_centre_cell_neibours(self):
        assert len(g.neibours((1,1), [[0,0,0],[0,0,0],[0,0,0],[0,0,0]])) == 4

class TestUnfloodedNeibours:
    def test_unflooded_neibours(self):
        tile = (1,1)
        map = [[0,1],
               [0,0]]
        flooded = [(1,0),(1,1)]
        ns = g.neibours(tile, map)
        assert g.unflooded_neibours(ns, flooded) == [(0,1)]
        
class TestFloodTile:
    def test_dont_flood_tile_at_same_height_as_water(self):
        tile = (0,0)
        map = [[0]]
        assert g.flood_tile(map, tile, 0) == []

    def test_dont_flood_tile_higher_than_water(self):
        tile = (0,0)
        map = [[1]]
        assert g.flood_tile(map, tile, 0) == []

    def test_flood_tile_lower_than_water(self):
        tile = (0,0)
        map = [[1]]
        assert g.flood_tile(map, tile, 2) == [(0,0)]

class TestFloodNeibours:
    def test_flood_one_neibour(self):
        tile = (1,1)
        map = [[0,1],
               [0,0]]
        flooded = [(1,1)]
        shore = list(flooded)
        assert g.flood_neibours(map, shore, flooded, 1, tile) == [(1,0)] 

    def test_flood_multiple_neibours(self):
        tile = (1,1)
        map = [[1,1,1],
               [1,0,2]]
        flooded = [(1,1)]
        shore = list(flooded)
        assert g.flood_neibours(map, shore, flooded, 2, tile) == [(0,1),(1,0)] 

